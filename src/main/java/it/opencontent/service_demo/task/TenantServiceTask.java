package it.opencontent.service_demo.task;

import it.opencontent.service_demo.entity.ServiceEntity;
import it.opencontent.service_demo.service.ITenantService;
import java.util.List;
import java.util.concurrent.Callable;

public class TenantServiceTask implements Callable<List<ServiceEntity>> {
    private final ITenantService tenantService;
    private final String tenantSlug;

    public TenantServiceTask(ITenantService tenantService, String tenantSlug){
        this.tenantService = tenantService;
        this.tenantSlug = tenantSlug;
    }

    @Override
    public List<ServiceEntity> call() throws Exception {
        return tenantService.getServices(this.tenantSlug);
    }
}

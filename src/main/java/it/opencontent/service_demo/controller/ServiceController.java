package it.opencontent.service_demo.controller;


import it.opencontent.service_demo.service.IResourceService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ExecutionException;


@RestController
@RequestMapping(path = "api/v1/services")
public class ServiceController {
    IResourceService resourceService;

    public ServiceController(IResourceService resourceService) {
        this.resourceService = resourceService;
    }

    @GetMapping
    public List<HashMap<String, String>> getServices() throws IOException, InterruptedException, ExecutionException {
        return resourceService.getServices();
    }
}

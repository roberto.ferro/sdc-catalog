package it.opencontent.service_demo.controller;

import it.opencontent.service_demo.entity.ServiceEntity;
import it.opencontent.service_demo.entity.TenantEntity;
import it.opencontent.service_demo.service.ITenantService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping(path = "api/v1/tenants")
public class TenantController {
    private final ITenantService tenantService;

    @Autowired
    public TenantController(ITenantService tenantService) {
        this.tenantService = tenantService;
    }

    @GetMapping
    public List<TenantEntity> getTenants() throws IOException {
        return tenantService.getTenants();
    }

    @GetMapping("/{tenantID}/services")
    public List<ServiceEntity> getServices(@PathVariable("tenantID") String tenantID) throws IOException {
        return tenantService.getServices(tenantID);
    }
}

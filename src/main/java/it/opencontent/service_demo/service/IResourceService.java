package it.opencontent.service_demo.service;

import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ExecutionException;

@Component
public interface IResourceService {
    List<HashMap<String, String>> getServices() throws IOException, InterruptedException, ExecutionException;
}

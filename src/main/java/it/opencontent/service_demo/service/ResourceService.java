package it.opencontent.service_demo.service;

import it.opencontent.service_demo.entity.ServiceEntity;
import it.opencontent.service_demo.entity.TenantEntity;
import it.opencontent.service_demo.task.TenantServiceTask;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.*;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadPoolExecutor;

@Component
@EnableCaching
public class ResourceService implements IResourceService {
    private final ITenantService tenantService;

    private final List<String> parsedServices = new ArrayList<>();
    private final List<HashMap<String, String>> services = new ArrayList<>();
    private final List<Future<List<ServiceEntity>>> resultList = new ArrayList<>();

    public ResourceService(ITenantService tenantService) {
        this.tenantService = tenantService;
    }

    @Override
    @Cacheable("available-services")
    public List<HashMap<String, String>> getServices() throws IOException, ExecutionException, InterruptedException {
        List<TenantEntity> tenants = tenantService.getTenants();
        ThreadPoolExecutor executor = (ThreadPoolExecutor) Executors.newCachedThreadPool();
        for(TenantEntity tenant: tenants){
            Future<List<ServiceEntity>> services = executor.submit(new TenantServiceTask(tenantService, tenant.getSlug()));
            resultList.add(services);
        }

        for(Future<List<ServiceEntity>> services: resultList){
            mapTenantServices(services.get());
        }

        return this.services;
    }

    private void mapTenantServices(List<ServiceEntity> services){
        for(ServiceEntity service: services){
            if (parsedServices.contains(service.getName())) {
                return;
            }

            parsedServices.add(service.getName());
            HashMap<String, String> result = new HashMap<>() {{
                put("id", service.getId());
                put("title", service.getName());
                put("description", service.getDescription());
            }};

            this.services.add(result);
        }
    }
}

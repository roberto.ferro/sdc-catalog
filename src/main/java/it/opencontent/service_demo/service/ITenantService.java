package it.opencontent.service_demo.service;

import it.opencontent.service_demo.entity.ServiceEntity;
import it.opencontent.service_demo.entity.TenantEntity;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.List;

@Component
public interface ITenantService {
    List<TenantEntity> getTenants() throws IOException;

    List<ServiceEntity> getServices(String tenantID) throws IOException;
}

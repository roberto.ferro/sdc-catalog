package it.opencontent.service_demo.service;

import it.opencontent.service_demo.api.ApiConfiguration;
import it.opencontent.service_demo.api.TenantAPI;
import it.opencontent.service_demo.entity.ServiceEntity;
import it.opencontent.service_demo.entity.TenantEntity;
import org.springframework.stereotype.Component;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import java.io.IOException;
import java.util.List;

@Component
public class TenantService implements ApiConfiguration, ITenantService {
    private final TenantAPI service;

    public TenantService() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(API_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        service = retrofit.create(TenantAPI.class);
    }

    @Override
    public List<TenantEntity> getTenants() throws IOException {
        Call<List<TenantEntity>> call = service.getTenants();
        Response<List<TenantEntity>> response = call.execute();

        if (!response.isSuccessful()) {
            throw new IOException(response.errorBody() != null
                    ? response.errorBody().string() : "Unknown error");
        }

        return response.body();
    }

    @Override
    public List<ServiceEntity> getServices(String tenantID) throws IOException {
        Call<List<ServiceEntity>> call = service.getServices(tenantID);
        Response<List<ServiceEntity>> response = call.execute();

        if (!response.isSuccessful()) {
            throw new IOException(response.errorBody() != null
                    ? response.errorBody().string() : "Unknown error");
        }

        return response.body();
    }
}

package it.opencontent.service_demo.entity;

import java.util.Map;

public class ServiceStepEntity {
    private String identifier;
    private String title;
    private String type;
    private String description;
    private String guide;
    private Map<String, String> parameters;

    public ServiceStepEntity(
            String identifier,
            String title,
            String type,
            String description,
            String guide,
            Map<String, String> parameters
    ) {
        this.identifier = identifier;
        this.title = title;
        this.type = type;
        this.description = description;
        this.guide = guide;
        this.parameters = parameters;
    }

    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getGuide() {
        return guide;
    }

    public void setGuide(String guide) {
        this.guide = guide;
    }

    public Map<String, String> getParameters() {
        return parameters;
    }

    public void setParameters(Map<String, String> parameters) {
        this.parameters = parameters;
    }
}

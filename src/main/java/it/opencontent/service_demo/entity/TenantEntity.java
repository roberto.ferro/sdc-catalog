package it.opencontent.service_demo.entity;

import java.util.List;
import java.util.Map;

public class TenantEntity {
    private List<String> targets;
    private Map<String, String> labels;

    public TenantEntity(List<String> targets, Map<String, String> labels) {
        this.targets = targets;
        this.labels = labels;
    }

    public List<String> getTargets() {
        return targets;
    }

    public void setTargets(List<String> targets) {
        this.targets = targets;
    }

    public Map<String, String> getLabels() {
        return labels;
    }

    public void setLabels(Map<String, String> labels) {
        this.labels = labels;
    }

    public String getSlug() {
        return this.getLabels().get("__metrics_path__")
                .replace("/metrics", "")
                .replaceAll("/", "");
    }
}

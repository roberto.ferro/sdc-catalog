package it.opencontent.service_demo.entity;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ServiceEntity {
    private String id;
    private String name;
    private String slug;
    private String tenant;
    private String description;
    private String howTo;
    private String who;
    private String specialCases;
    private String moreInfo;
    private String compilationInfo;
    private String finalIndications;
    private List<String> coverage;
    private String responseType;
    private List<ServiceStepEntity> flowSteps;
    private boolean protocolRequired;
    private String protocolHeader;
    private HashMap<String, String> protocolParameters;
    private boolean paymentRequired;
    private List<String> paymentParameters;
    private boolean sticky;
    private int status;
    private int accessLevel;
    private boolean loginSuggested;
    private String scheduledFrom;
    private String scheduledTo;
    private String serviceGroup;
    private boolean allowedReopening;
    private int workflow;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public String getTenant() {
        return tenant;
    }

    public void setTenant(String tenant) {
        this.tenant = tenant;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getHowTo() {
        return howTo;
    }

    public void setHowTo(String howTo) {
        this.howTo = howTo;
    }

    public String getWho() {
        return who;
    }

    public void setWho(String who) {
        this.who = who;
    }

    public String getSpecialCases() {
        return specialCases;
    }

    public void setSpecialCases(String specialCases) {
        this.specialCases = specialCases;
    }

    public String getMoreInfo() {
        return moreInfo;
    }

    public void setMoreInfo(String moreInfo) {
        this.moreInfo = moreInfo;
    }

    public String getCompilationInfo() {
        return compilationInfo;
    }

    public void setCompilationInfo(String compilationInfo) {
        this.compilationInfo = compilationInfo;
    }

    public String getFinalIndications() {
        return finalIndications;
    }

    public void setFinalIndications(String finalIndications) {
        this.finalIndications = finalIndications;
    }

    public List<String> getCoverage() {
        return coverage;
    }

    public void setCoverage(List<String> coverage) {
        this.coverage = coverage;
    }

    public String getResponseType() {
        return responseType;
    }

    public void setResponseType(String responseType) {
        this.responseType = responseType;
    }

    public List<ServiceStepEntity> getFlowSteps() {
        return flowSteps;
    }

    public void setFlowSteps(List<ServiceStepEntity> flowSteps) {
        this.flowSteps = flowSteps;
    }

    public boolean isProtocolRequired() {
        return protocolRequired;
    }

    public void setProtocolRequired(boolean protocolRequired) {
        this.protocolRequired = protocolRequired;
    }

    public String getProtocolHeader() {
        return protocolHeader;
    }

    public void setProtocolHeader(String protocolHeader) {
        this.protocolHeader = protocolHeader;
    }

    public Map<String, String> getProtocolParameters() {
        return protocolParameters;
    }

    public void setProtocolParameters(HashMap<String, String> protocolParameters) {
        this.protocolParameters = protocolParameters;
    }

    public boolean isPaymentRequired() {
        return paymentRequired;
    }

    public void setPaymentRequired(boolean paymentRequired) {
        this.paymentRequired = paymentRequired;
    }

    public List<String> getPaymentParameters() {
        return paymentParameters;
    }

    public void setPaymentParameters(List<String> paymentParameters) {
        this.paymentParameters = paymentParameters;
    }

    public boolean isSticky() {
        return sticky;
    }

    public void setSticky(boolean sticky) {
        this.sticky = sticky;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getAccessLevel() {
        return accessLevel;
    }

    public void setAccessLevel(int accessLevel) {
        this.accessLevel = accessLevel;
    }

    public boolean isLoginSuggested() {
        return loginSuggested;
    }

    public void setLoginSuggested(boolean loginSuggested) {
        this.loginSuggested = loginSuggested;
    }

    public String getScheduledFrom() {
        return scheduledFrom;
    }

    public void setScheduledFrom(String scheduledFrom) {
        this.scheduledFrom = scheduledFrom;
    }

    public String getScheduledTo() {
        return scheduledTo;
    }

    public void setScheduledTo(String scheduledTo) {
        this.scheduledTo = scheduledTo;
    }

    public String getServiceGroup() {
        return serviceGroup;
    }

    public void setServiceGroup(String serviceGroup) {
        this.serviceGroup = serviceGroup;
    }

    public boolean isAllowedReopening() {
        return allowedReopening;
    }

    public void setAllowedReopening(boolean allowedReopening) {
        this.allowedReopening = allowedReopening;
    }

    public int getWorkflow() {
        return workflow;
    }

    public void setWorkflow(int workflow) {
        this.workflow = workflow;
    }

    public ServiceEntity(
            String id, String name, String slug, String tenant, String description, String howTo, String who,
            String specialCases, String moreInfo, String compilationInfo, String finalIndications,
            List<String> coverage, String responseType, List<ServiceStepEntity> flowSteps, boolean protocolRequired,
            String protocolHeader, HashMap<String, String> protocolParameters, boolean paymentRequired,
            List<String> paymentParameters, boolean sticky, int status, int accessLevel, boolean loginSuggested,
            String scheduledFrom, String scheduledTo, String serviceGroup, boolean allowedReopening, int workflow
    ) {
        this.id = id;
        this.name = name;
        this.slug = slug;
        this.tenant = tenant;
        this.description = description;
        this.howTo = howTo;
        this.who = who;
        this.specialCases = specialCases;
        this.moreInfo = moreInfo;
        this.compilationInfo = compilationInfo;
        this.finalIndications = finalIndications;
        this.coverage = coverage;
        this.responseType = responseType;
        this.flowSteps = flowSteps;
        this.protocolRequired = protocolRequired;
        this.protocolHeader = protocolHeader;
        this.protocolParameters = protocolParameters;
        this.paymentRequired = paymentRequired;
        this.paymentParameters = paymentParameters;
        this.sticky = sticky;
        this.status = status;
        this.accessLevel = accessLevel;
        this.loginSuggested = loginSuggested;
        this.scheduledFrom = scheduledFrom;
        this.scheduledTo = scheduledTo;
        this.serviceGroup = serviceGroup;
        this.allowedReopening = allowedReopening;
        this.workflow = workflow;
    }
}

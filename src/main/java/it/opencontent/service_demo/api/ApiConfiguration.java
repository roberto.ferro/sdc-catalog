package it.opencontent.service_demo.api;

public interface ApiConfiguration {
    String API_BASE_URL = "https://www2.stanzadelcittadino.it/";
}

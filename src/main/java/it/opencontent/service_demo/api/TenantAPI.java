package it.opencontent.service_demo.api;

import it.opencontent.service_demo.entity.ServiceEntity;
import it.opencontent.service_demo.entity.TenantEntity;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

import java.util.List;

public interface TenantAPI {

    @GET("/prometheus.json")
    Call<List<TenantEntity>> getTenants();

    @GET("/{tenantID}/api/services")
    Call<List<ServiceEntity>> getServices(@Path("tenantID") String tenantID);
}

package it.opencontent.service_demo.api;

import io.reactivex.Observable;
import it.opencontent.service_demo.entity.ServiceEntity;
import it.opencontent.service_demo.entity.TenantEntity;
import retrofit2.http.GET;
import retrofit2.http.Path;

import java.util.List;

public interface TenantRxAPI {

    @GET("/prometheus.json")
    Observable<List<TenantEntity>> getTenants();

    @GET("/{tenantID}/api/services")
    Observable<List<ServiceEntity>> getServices(@Path("tenantID") String tenantID);
}

FROM openjdk:11.0.6-slim
EXPOSE 8080
ADD "target/service_demo-0.0.1-SNAPSHOT.jar" "service_demo-0.0.1-SNAPSHOT.jar"
ENTRYPOINT ["java","-jar", "/service_demo-0.0.1-SNAPSHOT.jar"]